import { Customer } from "../../../domain/customer/entity/customer";
import { CustomerRepositoryInterface } from "../../../domain/customer/repository/customer.repository.interface";
import { Address } from "../../../domain/customer/value-object/address";
import { CustomerModel } from "./sequelize/customer.model";

export class CustomerRepository implements CustomerRepositoryInterface {
  async create(entity: Customer): Promise<void> {
    await CustomerModel.create({
      id: entity.id,
      name: entity.name,
      street: entity.address.street,
      number: entity.address.number,
      zipCode: entity.address.zip,
      city: entity.address.city,
      active: entity.isActive(),
      rewardPoints: entity.rewardPoints,
    });
  }

  async update(id: string, entity: Customer): Promise<void> {
    await CustomerModel.update(
      {
        name: entity.name,
        street: entity.address.street,
        number: entity.address.number,
        zipCode: entity.address.zip,
        city: entity.address.city,
        active: entity.isActive(),
        rewardPoints: entity.rewardPoints,
      },
      { where: { id } }
    );
  }

  delete(id: string): Promise<void> {
    throw new Error("Method not implemented.");
  }

  async find(id: string): Promise<Customer> {
    try {
      const customerFound = await CustomerModel.findOne({
        where: { id },
        rejectOnEmpty: true,
      });
      const customer = new Customer(customerFound.id, customerFound.name);
      const address = new Address(
        customerFound.street,
        customerFound.city,
        customerFound.number,
        customerFound.zipCode
      );

      customer.changeAddress(address);
      customer.addRewardPoints(customerFound.rewardPoints);

      return customer;
    } catch (error) {
      throw new Error("Customer not found");
    }
  }

  async findAll(): Promise<Customer[]> {
    try {
      const customersFound = await CustomerModel.findAll();
      return customersFound.map((customerFound) => {
        const customer = new Customer(customerFound.id, customerFound.name);
        const address = new Address(
          customerFound.street,
          customerFound.city,
          customerFound.number,
          customerFound.zipCode
        );

        customer.changeAddress(address);
        customer.addRewardPoints(customerFound.rewardPoints);

        return customer;
      });
    } catch (error) {
      throw new Error("Customer not found");
    }
  }
}
