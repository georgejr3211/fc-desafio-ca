import { Sequelize } from 'sequelize-typescript';
import { Customer } from '../../../domain/customer/entity/customer';
import { Address } from '../../../domain/customer/value-object/address';

import { CustomerRepository } from './customer.repository';
import { CustomerModel } from './sequelize/customer.model';

describe('Customer Repository unit test', () => {

    let sequelize: Sequelize;

    beforeEach(async () => {
        sequelize = new Sequelize({
            dialect: 'sqlite',
            storage: ':memory:',
            logging: false,
            sync: { force: true },
        });

        sequelize.addModels([CustomerModel]);

        await sequelize.sync();
    });

    afterEach(async () => {
        await sequelize.close();
    });

    it('should create a customer', async () => {
        const customerRepository = new CustomerRepository();
        const customer = new Customer('123', 'John Doe');
        const address = new Address('street', 'number', 'zipCode', 'city');
        customer.changeAddress(address);

        await customerRepository.create(customer);

        const customerFound = await CustomerModel.findOne({ where: { id: customer.id } });

        expect(customerFound.toJSON()).toStrictEqual({
            id: customer.id,
            name: customer.name,
            street: customer.address.street,
            number: customer.address.number,
            zipCode: customer.address.zip,
            city: customer.address.city,
            active: customer.isActive(),
            rewardPoints: customer.rewardPoints,
        });
    });

    it('should update a customer', async () => {
        const customerRepository = new CustomerRepository();
        const customer = new Customer('123', 'John Doe');
        const address = new Address('street', 'number', 'zipCode', 'city');
        customer.changeAddress(address);

        await customerRepository.create(customer);

        const customerFound = await CustomerModel.findOne({ where: { id: customer.id } });

        expect(customerFound.toJSON()).toStrictEqual({
            id: customer.id,
            name: customer.name,
            street: customer.address.street,
            number: customer.address.number,
            zipCode: customer.address.zip,
            city: customer.address.city,
            active: customer.isActive(),
            rewardPoints: customer.rewardPoints,
        });

        customer.changeName('John Doe 2');
        customer.changeAddress(new Address('street 2', 'number 2', 'zipCode 2', 'city 2'));
        customer.activate();

        await customerRepository.update(customer.id, customer);

        const customerFoundUpdated = await CustomerModel.findOne({ where: { id: customer.id } });

        expect(customerFoundUpdated.toJSON()).toStrictEqual({
            id: customer.id,
            name: customer.name,
            street: customer.address.street,
            number: customer.address.number,
            zipCode: customer.address.zip,
            city: customer.address.city,
            active: customer.isActive(),
            rewardPoints: customer.rewardPoints,
        });
    });

    it('should find a customer', async () => {
        const customerRepository = new CustomerRepository();
        const customer = new Customer('123', 'John Doe');
        const address = new Address('street 2', 'number 2', 'zipCode 2', 'city 2');
        customer.changeAddress(address);

        await customerRepository.create(customer);

        const customerFound = await customerRepository.find(customer.id);

        expect(customerFound).toEqual(customer);
    });

    it('should throw an error when customer not found', () => {
        const customerRepository = new CustomerRepository();

        expect(async () => {
            await customerRepository.find('123');
        }).rejects.toThrowError('Customer not found');
    });

});