import { Order } from '../../../domain/checkout/entity/order';
import { OrderItem } from '../../../domain/checkout/entity/order-item';
import { OrderRepositoryInterface } from '../../../domain/checkout/repository/order.repository.interface';
import { OrderItemModel } from './sequelize/order-item.model';
import { OrderModel } from './sequelize/order.model';

export class OrderRepository implements OrderRepositoryInterface {
    async create(entity: Order): Promise<void> {
        await OrderModel.create(
            {
                id: entity.id,
                customer_id: entity.customerId,
                total: entity.total(),
                items: entity.items.map((item) => ({
                    id: item.id,
                    name: item.name,
                    price: item.price,
                    product_id: item.productId,
                    quantity: item.quantity,
                })),
            },
            {
                include: ['items'],
            }
        );
    }

    async update(id: string, entity: Order): Promise<void> {

        await OrderModel.update(
            {
                id: entity.id,
                custumer_id: entity.customerId,
                total: entity.total(),
            },
            {
                where: { id }
            }
        );

        entity.items.forEach(async item => {
            await OrderItemModel.update({
                id: item.id,
                name: item.name,
                price: item.price,
                product_id: item.productId,
                quantity: item.quantity,
            },
                {
                    where: { id: item.id }
                })
        });
    }

    async find(id: string): Promise<Order> {
        const foundOrder = await OrderModel.findOne(
            {
                where: { id: id },
                include: ['items']
            }
        );

        return new Order(foundOrder.id, foundOrder.customer_id, foundOrder.items.map(item => {
            return new OrderItem(item.id, item.name, item.price, item.product_id, item.quantity);
        }));

    }

    async findAll(): Promise<Order[]> {
        const foundOrders = await OrderModel.findAll(
            {
                include: ['items']
            }
        );
        return foundOrders.map(order => {
            return new Order(order.id, order.customer_id, order.items.map(item => {
                return new OrderItem(item.id, item.name, item.price, item.product_id, item.quantity);
            }))
        })
    }

    async delete(id: string): Promise<void> {
        await OrderModel.destroy({
            where: { id }
        });
    }
}