import { Sequelize } from 'sequelize-typescript';
import { Order } from '../../../domain/checkout/entity/order';
import { OrderItem } from '../../../domain/checkout/entity/order-item';
import { Customer } from '../../../domain/customer/entity/customer';
import { Address } from '../../../domain/customer/value-object/address';
import { Product } from '../../../domain/product/entity/product';
import { CustomerRepository } from '../../customer/repository/customer.repository';
import { CustomerModel } from '../../customer/repository/sequelize/customer.model';
import { ProductRepository } from '../../product/repository/product.repository';
import { ProductModel } from '../../product/repository/sequelize/product.model';
import { OrderRepository } from './order.repository';
import { OrderItemModel } from './sequelize/order-item.model';
import { OrderModel } from './sequelize/order.model';

describe('Order Repository unit test', () => {

    let sequelize: Sequelize;

    beforeEach(async () => {
        sequelize = new Sequelize({
            dialect: 'sqlite',
            storage: ':memory:',
            logging: false,
            sync: { force: true },
        });

        sequelize.addModels([OrderModel, OrderItemModel, ProductModel, CustomerModel]);

        await sequelize.sync();
    });

    afterEach(async () => {
        await sequelize.close();
    });

    it('should create a order', async () => {
        const customerRepository = new CustomerRepository();
        const customer = new Customer('123', 'John Doe');
        const address = new Address('street', 'number', 'zipCode', 'city');
        customer.changeAddress(address);

        await customerRepository.create(customer);

        const productRepository = new ProductRepository();
        const product = new Product('123', 'Product 1', 10);

        await productRepository.create(product);

        const orderItem = new OrderItem('1', product.name, product.price, product.id, 10);

        const orderRepository = new OrderRepository();
        const order = new Order('123', customer.id, [orderItem]);

        await orderRepository.create(order);

        const orderFound = await OrderModel.findOne({ where: { id: order.id }, include: ['items'] });

        expect(orderFound.toJSON()).toStrictEqual({
            id: order.id,
            customer_id: order.customerId,
            total: order.total(),
            items: [
                {
                    id: orderItem.id,
                    order_id: order.id,
                    product_id: orderItem.productId,
                    name: orderItem.name,
                    price: orderItem.price,
                    quantity: orderItem.quantity,
                }
            ]
        });
    });

    it('should update a order', async () => {
        const customerRepository = new CustomerRepository();
        const customer = new Customer('123', 'John Doe');
        const address = new Address('street', 'number', 'zipCode', 'city');
        customer.changeAddress(address);

        await customerRepository.create(customer);

        const productRepository = new ProductRepository();
        const product1 = new Product('1', 'Product 1', 10);
        const product2 = new Product('2', 'Product 2', 30);

        await productRepository.create(product1);
        await productRepository.create(product2);

        const orderItem = new OrderItem('1', product1.name, product1.price, product1.id, 10);
        const orderItem2 = new OrderItem('2', product2.name, product2.price, product2.id, 10);

        const orderRepository = new OrderRepository();
        const order = new Order('123', customer.id, [orderItem, orderItem2]);

        await orderRepository.create(order);

        orderItem.changeQuantity(20);

        await orderRepository.update(order.id, order);

        const orderFound = await OrderModel.findOne({ where: { id: order.id }, include: ['items'] });

        expect(orderFound.toJSON()).toStrictEqual({
            id: order.id,
            customer_id: order.customerId,
            total: order.total(),
            items: [
                {
                    id: orderItem.id,
                    order_id: order.id,
                    product_id: orderItem.productId,
                    name: orderItem.name,
                    price: orderItem.price,
                    quantity: orderItem.quantity,
                },
                {
                    id: orderItem2.id,
                    order_id: order.id,
                    product_id: orderItem2.productId,
                    name: orderItem2.name,
                    price: orderItem2.price,
                    quantity: orderItem2.quantity,
                }
            ]
        });
    });

    it('should find a order', async () => {
        const customerRepository = new CustomerRepository();
        const customer = new Customer('123', 'John Doe');
        const address = new Address('street', 'city', '145', 'zipCode');
        customer.changeAddress(address);

        await customerRepository.create(customer);

        const productRepository = new ProductRepository();
        const product = new Product('123', 'Product 1', 10);

        await productRepository.create(product);

        const orderItem = new OrderItem('1', product.name, product.price, product.id, 1);

        const orderRepository = new OrderRepository();
        const order = new Order('123', customer.id, [orderItem]);

        await orderRepository.create(order);

        const orderFound = await orderRepository.find(order.id);

        expect(orderFound).toStrictEqual(order);
    });

    it('should find all orders', async () => {
        const customerRepository = new CustomerRepository();
        const customer = new Customer('123', 'John Doe');
        const address = new Address('street', 'city', '145', 'zipCode');
        customer.changeAddress(address);

        await customerRepository.create(customer);

        const productRepository = new ProductRepository();
        const product = new Product('123', 'Product 1', 10);

        await productRepository.create(product);

        const orderItem = new OrderItem('1', product.name, product.price, product.id, 10);

        const orderRepository = new OrderRepository();
        const order = new Order('123', customer.id, [orderItem]);

        await orderRepository.create(order);

        const ordersFound = await orderRepository.findAll();

        expect(ordersFound).toHaveLength(1);
    });

    it('should delete a order', async () => {
        const customerRepository = new CustomerRepository();
        const customer = new Customer('123', 'John Doe');
        const address = new Address('street', 'city', '145', 'zipCode');
        customer.changeAddress(address);

        await customerRepository.create(customer);

        const productRepository = new ProductRepository();
        const product = new Product('123', 'Product 1', 10);

        await productRepository.create(product);

        const orderItem = new OrderItem('1', product.name, product.price, product.id, 10);

        const orderRepository = new OrderRepository();
        const order = new Order('123', customer.id, [orderItem]);

        await orderRepository.create(order);

        await orderRepository.delete(order.id);

        const ordersFound = await orderRepository.findAll();

        expect(ordersFound).toHaveLength(0);
    });
});