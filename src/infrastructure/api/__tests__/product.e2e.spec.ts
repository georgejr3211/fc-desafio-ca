import { app, sequelize } from "../express";
import request from "supertest";

describe("E2E test for product", () => {
  beforeEach(async () => {
    await sequelize.sync({ force: true });
  });

  afterAll(async () => {
    await sequelize.close();
  });

  it("should create a product", async () => {
    const response = await request(app).post("/product").send({
      name: "Product 1",
      price: 10.99,
    });

    expect(response.status).toBe(201);
    expect(response.body.id).toBeDefined();
    expect(response.body.name).toBe("Product 1");
    expect(response.body.price).toBe(10.99);
  });

  it("should list all products", async () => {
    const response1 = await request(app).post("/product").send({
      name: "Product 1",
      price: 10.99,
    });

    const response2 = await request(app).post("/product").send({
      name: "Product 2",
      price: 50.25,
    });

    const listResponse = await request(app).get("/product");
    expect(listResponse.status).toBe(200);
    expect(listResponse.body.products.length).toBe(2);
    expect(listResponse.body.products[0].id).toBe(response1.body.id);
    expect(listResponse.body.products[0].name).toBe("Product 1");
    expect(listResponse.body.products[0].price).toBe(10.99);
    expect(listResponse.body.products[1].id).toBe(response2.body.id);
    expect(listResponse.body.products[1].name).toBe("Product 2");
    expect(listResponse.body.products[1].price).toBe(50.25);
  });
});
