import { CreateCustomerUseCase } from "./../../../usecase/customer/create/create.customer.usecase";
import { Router, Request, Response } from "express";
import { CustomerRepository } from "../../customer/repository/customer.repository";
import { CustomerRepositoryInterface } from "../../../domain/customer/repository/customer.repository.interface";
import { InputCreateCustomerDto } from "../../../usecase/customer/create/create.customer.dto";
import { ListCustomerUseCase } from "../../../usecase/customer/list/list.customer.usecase";
import { InputListCustomerDto } from "../../../usecase/customer/list/list.customer.dto";
import { CustomerPresenter } from "../presenters/customer.presenter";

export const customerRoute = Router();

customerRoute.post("/", async (req: Request, res: Response) => {
  const repository: CustomerRepositoryInterface = new CustomerRepository();
  const usecase = new CreateCustomerUseCase(repository);

  try {
    const customerDto: InputCreateCustomerDto = {
      name: req.body.name,
      address: {
        street: req.body.address.street,
        city: req.body.address.city,
        number: req.body.address.number,
        zip: req.body.address.zip,
      },
    };

    const output = await usecase.execute(customerDto);
    res.status(201).json(output);
  } catch (error) {
    res.status(400).json(error);
  }
});

customerRoute.get("/", async (req: Request, res: Response) => {
  try {
    const repository: CustomerRepositoryInterface = new CustomerRepository();
    const usecase = new ListCustomerUseCase(repository);

    const input: InputListCustomerDto = {};
    const output = await usecase.execute(input);

    res.format({
      json: async () => res.send(output),
      xml: async () => res.send(CustomerPresenter.listXML(output)),
    });
  } catch (error) {
    res.status(400).json(error);
  }
});
