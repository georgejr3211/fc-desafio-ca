import { Request, Response, Router } from "express";

import { ProductRepositoryInterface } from "../../../domain/product/repository/product.repository.interface";
import { InputCreateProductDto } from "../../../usecase/product/create/create.product.dto";
import { CreateProductUseCase } from "../../../usecase/product/create/create.product.usecase";
import {
  InputListProductDto,
  OutputListProductDto,
} from "../../../usecase/product/list/list.product.dto";
import { ListProductUseCase } from "../../../usecase/product/list/list.product.usecase";
import { ProductRepository } from "../../product/repository/product.repository";

export const productRoute = Router();

productRoute.post("/", async (req: Request, res: Response) => {
  const repository: ProductRepositoryInterface = new ProductRepository();
  const usecase = new CreateProductUseCase(repository);

  try {
    const input: InputCreateProductDto = {
      name: req.body.name,
      price: req.body.price,
    };

    const output = await usecase.execute(input);
    res.status(201).json(output);
  } catch (error) {
    res.status(400).json(error);
  }
});

productRoute.get("/", async (req: Request, res: Response) => {
  const repository: ProductRepositoryInterface = new ProductRepository();
  const usecase = new ListProductUseCase(repository);

  try {
    const input: InputListProductDto = {
      name: req.body.name,
      price: req.body.price,
    };

    const output: OutputListProductDto = await usecase.execute(input);
    res.status(200).json(output);
  } catch (error) {
    res.status(400).json(error);
  }
});
