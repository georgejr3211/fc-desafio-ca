import { Sequelize } from "sequelize-typescript";

import { Product } from "../../../domain/product/entity/product";
import { ProductRepository } from "../../../infrastructure/product/repository/product.repository";
import { ProductModel } from "../../../infrastructure/product/repository/sequelize/product.model";
import { InputUpdateProductDto } from "./update.product.dto";
import { UpdateProductUseCase } from "./update.product.usecase";

const product = new Product("123", "product 1", 10);

const input: InputUpdateProductDto = {
  id: product.id,
  name: "product 2",
  price: 20,
};

describe("Integration test for update product", () => {
  let sequelize: Sequelize;

  beforeEach(async () => {
    sequelize = new Sequelize({
      dialect: "sqlite",
      storage: ":memory:",
      logging: false,
      sync: { force: true },
    });

    sequelize.addModels([ProductModel]);

    await sequelize.sync();
  });

  afterEach(async () => {
    await sequelize.close();
  });

  it("should update a product", async () => {
    const productRepository = new ProductRepository();

    await productRepository.create(product);

    const productUpdateUseCase = new UpdateProductUseCase(productRepository);

    const output = await productUpdateUseCase.execute(input);

    expect(output).toEqual(input);
  });
});
