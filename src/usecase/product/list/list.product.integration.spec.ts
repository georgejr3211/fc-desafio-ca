import { Sequelize } from "sequelize-typescript";

import { Product } from "../../../domain/product/entity/product";
import { ProductRepository } from "../../../infrastructure/product/repository/product.repository";
import { ProductModel } from "../../../infrastructure/product/repository/sequelize/product.model";
import { InputListProductDto, OutputListProductDto } from "./list.product.dto";
import { ListProductUseCase } from "./list.product.usecase";

const product1 = new Product("123", "product1", 100);
const product2 = new Product("456", "product2", 200);

describe("Integration test for find all products", () => {
  let sequelize: Sequelize;

  beforeEach(async () => {
    sequelize = new Sequelize({
      dialect: "sqlite",
      storage: ":memory:",
      logging: false,
      sync: { force: true },
    });

    sequelize.addModels([ProductModel]);

    await sequelize.sync();
  });

  afterEach(async () => {
    await sequelize.close();
  });

  it("should find all products", async () => {
    const repository = new ProductRepository();

    await repository.create(product1);
    await repository.create(product2);

    const usecase = new ListProductUseCase(repository);

    const input: InputListProductDto = {};
    const output: OutputListProductDto = await usecase.execute(input);

    expect(output.products.length).toBe(2);
    expect(output.products[0].id).toBe(product1.id);
    expect(output.products[0].name).toBe(product1.name);
    expect(output.products[0].price).toBe(product1.price);

    expect(output.products[1].id).toBe(product2.id);
    expect(output.products[1].name).toBe(product2.name);
    expect(output.products[1].price).toBe(product2.price);
  });
});
