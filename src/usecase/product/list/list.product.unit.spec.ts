import { Product } from "../../../domain/product/entity/product";
import { InputListProductDto, OutputListProductDto } from "./list.product.dto";
import { ListProductUseCase } from "./list.product.usecase";

const product1 = new Product("123", "product1", 100);
const product2 = new Product("456", "product2", 200);

const products = [product1, product2];

export const MockRepository = () => {
  return {
    find: jest.fn(),
    findAll: jest.fn().mockReturnValue(Promise.resolve(products)),
    create: jest.fn(),
    update: jest.fn(),
    delete: jest.fn(),
  };
};

describe("Unit test for find all products", () => {
  it("should find all products", async () => {
    const repository = MockRepository();
    const usecase = new ListProductUseCase(repository);

    const input: InputListProductDto = {};
    const output: OutputListProductDto = await usecase.execute(input);

    expect(output.products.length).toBe(2);
    expect(output.products[0].id).toBe(product1.id);
    expect(output.products[0].name).toBe(product1.name);
    expect(output.products[0].price).toBe(product1.price);

    expect(output.products[1].id).toBe(product2.id);
    expect(output.products[1].name).toBe(product2.name);
    expect(output.products[1].price).toBe(product2.price);
  });
});
