import { Sequelize } from "sequelize-typescript";

import { Product } from "../../../domain/product/entity/product";
import { ProductRepository } from "../../../infrastructure/product/repository/product.repository";
import { ProductModel } from "../../../infrastructure/product/repository/sequelize/product.model";
import { InputFindProductDto } from "./find.product.dto";
import { FindProductUseCase } from "./find.product.usecase";

const product = new Product("123", "product 1", 10);

const input: InputFindProductDto = {
  id: "123",
};

describe("Integration test for find product", () => {
  let sequelize: Sequelize;

  beforeEach(async () => {
    sequelize = new Sequelize({
      dialect: "sqlite",
      storage: ":memory:",
      logging: false,
      sync: { force: true },
    });

    sequelize.addModels([ProductModel]);

    await sequelize.sync();
  });

  afterEach(async () => {
    await sequelize.close();
  });

  it("shoud find a product", async () => {
    const repository = new ProductRepository();

    await repository.create(product);

    const usecase = new FindProductUseCase(repository);

    const output = await usecase.execute(input);

    expect(output).toEqual({
      id: product.id,
      name: product.name,
      price: product.price,
    });
  });

  it("should throw an error when product not found", async () => {
    const repository = new ProductRepository();
    const usecase = new FindProductUseCase(repository);

    input.id = "432";

    await expect(usecase.execute(input)).rejects.toThrow("Product not found");
  });
});
