import { Product } from "../../../domain/product/entity/product";
import { InputFindProductDto } from "./find.product.dto";
import { FindProductUseCase } from "./find.product.usecase";

const product = new Product("123", "product 1", 10);

const MockRepository = () => {
  return {
    find: jest.fn().mockReturnValue(Promise.resolve(product)),
    findAll: jest.fn(),
    create: jest.fn(),
    update: jest.fn(),
    delete: jest.fn(),
  };
};

const input: InputFindProductDto = {
  id: "123",
};

describe("Unit test for find product", () => {
  it("shoud find a product", async () => {
    const repository = MockRepository();
    const usecase = new FindProductUseCase(repository);

    const output = await usecase.execute(input);

    expect(output).toEqual({
      id: product.id,
      name: product.name,
      price: product.price,
    });
  });

  it("should throw an error when product not found", async () => {
    const repository = MockRepository();
    repository.find.mockImplementation(() => {
      throw new Error("Product not found");
    });
    const usecase = new FindProductUseCase(repository);

    input.id = "432";

    await expect(usecase.execute(input)).rejects.toThrow("Product not found");
  });
});
