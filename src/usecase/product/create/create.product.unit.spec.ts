import RepositoryInterface from "../../../domain/@shared/repository/repository.interface";
import { Product } from "../../../domain/product/entity/product";
import {
  InputCreateProductDto,
  OutputCreateProductDto,
} from "./create.product.dto";
import { CreateProductUseCase } from "./create.product.usecase";

const input: InputCreateProductDto = {
  name: "product 1",
  price: 10,
};

const MockRepository = () => {
  return {
    find: jest.fn(),
    findAll: jest.fn(),
    create: jest.fn(),
    update: jest.fn(),
    delete: jest.fn(),
  };
};

describe("Unit test for create product", () => {
  it("shoud create a product", async () => {
    const repository: RepositoryInterface<Product> = MockRepository();
    const usecase = new CreateProductUseCase(repository);

    const output: OutputCreateProductDto = await usecase.execute(input);

    expect(output).toEqual({
      id: expect.any(String),
      name: input.name,
      price: input.price,
    });
  });
});
