import { Sequelize } from "sequelize-typescript";

import RepositoryInterface from "../../../domain/@shared/repository/repository.interface";
import { Product } from "../../../domain/product/entity/product";
import { ProductRepository } from "../../../infrastructure/product/repository/product.repository";
import { ProductModel } from "../../../infrastructure/product/repository/sequelize/product.model";
import {
  InputCreateProductDto,
  OutputCreateProductDto,
} from "./create.product.dto";
import { CreateProductUseCase } from "./create.product.usecase";

const input: InputCreateProductDto = {
  name: "product 1",
  price: 10,
};

describe("Integration test for create product", () => {
  let sequelize: Sequelize;

  beforeEach(async () => {
    sequelize = new Sequelize({
      dialect: "sqlite",
      storage: ":memory:",
      logging: false,
      sync: { force: true },
    });

    sequelize.addModels([ProductModel]);

    await sequelize.sync();
  });

  afterEach(async () => {
    await sequelize.close();
  });

  it("shoud create a product", async () => {
    const repository: RepositoryInterface<Product> = new ProductRepository();
    const usecase = new CreateProductUseCase(repository);

    const output: OutputCreateProductDto = await usecase.execute(input);

    expect(output).toEqual({
      id: expect.any(String),
      name: input.name,
      price: input.price,
    });
  });
});
