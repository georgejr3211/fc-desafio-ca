import { InputCreateCustomerDto, OutputCreateCustomerDto } from "./create.customer.dto";
import { CreateCustomerUseCase } from "./create.customer.usecase";

const input: InputCreateCustomerDto = {
    name: 'John Doe',
    address: {
        street: 'Street',
        city: 'City',
        number: 'Number',
        zip: 'Zip',
    },
};

const MockRepository = () => {
    return {
        find: jest.fn(),
        findAll: jest.fn(),
        create: jest.fn(),
        update: jest.fn(),
        delete: jest.fn()
    }
};

describe('Unit Test create customer use case', () => {
    it('should create customer', async () => {
        const customerRepository = MockRepository();
        const useCase = new CreateCustomerUseCase(customerRepository);

        const output: OutputCreateCustomerDto = {
            id: expect.any(String),
            name: input.name,
            address: {
                street: input.address.street,
                city: input.address.city,
                number: input.address.number,
                zip: input.address.zip,
            },
        };

        const result: OutputCreateCustomerDto = await useCase.execute(input);

        expect(result).toEqual(output);
    });

    it('should thrown an error when name is missing', async () => {
        const customerRepository = MockRepository();
        const useCase = new CreateCustomerUseCase(customerRepository);

        input.name = '';

        await expect(useCase.execute(input)).rejects.toThrow('customer: must have a name');
    });

    it('should thrown an error when street is missing', async () => {
        const customerRepository = MockRepository();
        const useCase = new CreateCustomerUseCase(customerRepository);

        input.address.street = '';

        await expect(useCase.execute(input)).rejects.toThrow('Address must have a street');
    });
});