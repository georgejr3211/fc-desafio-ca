import { CustomerRepositoryInterface } from "../../../domain/customer/repository/customer.repository.interface";
import { Address } from "../../../domain/customer/value-object/address";
import {
  InputUpdateCustomerDto,
  OutputUpdateCustomerDto,
} from "./update.customer.dto";

export class UpdateCustomerUseCase {
  constructor(
    private readonly customerRepository: CustomerRepositoryInterface
  ) {}

  async execute(
    input: InputUpdateCustomerDto
  ): Promise<OutputUpdateCustomerDto> {
    const customer = await this.customerRepository.find(input.id);
    customer.changeName(input.name);
    customer.changeAddress(
      new Address(
        input.address.street,
        input.address.city,
        input.address.number.toString(),
        input.address.zip
      )
    );

    await this.customerRepository.update(customer.id, customer);

    return {
      id: customer.id,
      name: customer.name,
      address: {
        street: customer.address.street,
        number: parseInt(customer.address.number),
        zip: customer.address.zip,
        city: customer.address.city,
      },
    };
  }
}
