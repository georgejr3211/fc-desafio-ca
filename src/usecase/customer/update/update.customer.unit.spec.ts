import { UpdateCustomerUseCase } from './update.customer.usecase';
import { CustomerFactory } from "../../../domain/customer/factory/customer.factory";
import { Address } from "../../../domain/customer/value-object/address";
import {
  InputUpdateCustomerDto,
  OutputUpdateCustomerDto,
} from "./update.customer.dto";

const customer = CustomerFactory.createWithAddress(
  "John",
  new Address("Street", "City", "Number", "Zip")
);

const input: InputUpdateCustomerDto = {
  id: customer.id,
  name: "John Updated",
  address: {
    street: "Street Updated",
    number: 1234,
    zip: "Zip updated",
    city: "City updated",
  },
};

const MockRepository = () => {
  return {
    find: jest.fn().mockReturnValue(Promise.resolve(customer)),
    findAll: jest.fn(),
    create: jest.fn(),
    update: jest.fn(),
    delete: jest.fn(),
  };
};

describe("Customer update unit case", () => {
  it("should updated a customer", async () => {
    const customerRepository = MockRepository();
    const customerUpdateUseCase = new UpdateCustomerUseCase(customerRepository);

    const output: OutputUpdateCustomerDto = await customerUpdateUseCase.execute(
      input
    );

    expect(output).toEqual(input);
  });
});
