import { Customer } from '../../../domain/customer/entity/customer';
import { Address } from '../../../domain/customer/value-object/address';
import { InputFindCustomerDto, OutputFindCustomerDto } from './find.customer.dto';
import { FindCustomerUseCase } from './find.customer.usecase';

const customer = new Customer('123', 'John Doe');
const address = new Address('Street', 'City', 'Number', 'Zip');
customer.changeAddress(address);

const MockRepository = () => {
    return {
        find: jest.fn().mockReturnValue(Promise.resolve(customer)),
        findAll: jest.fn(),
        create: jest.fn(),
        update: jest.fn(),
        delete: jest.fn()
    }
}

describe('Unit Test find customer use case', () => {
    it('should find customer', async () => {
        const customerRepository = MockRepository();
        await customerRepository.create(customer);


        const useCase = new FindCustomerUseCase(customerRepository);

        const input: InputFindCustomerDto = {
            id: '123',
        };

        const output: OutputFindCustomerDto = {
            id: '123',
            name: 'John Doe',
            address: {
                street: 'Street',
                city: 'City',
                number: 'Number',
                zip: 'Zip',
            },
        }

        const result: OutputFindCustomerDto = await useCase.execute(input);

        expect(result).toEqual(output);
    });

    it('should throw error when customer not found', async () => {
        const customerRepository = MockRepository();
        customerRepository.find.mockImplementation(() => {
            throw new Error('Customer not found');
        });

        const useCase = new FindCustomerUseCase(customerRepository);

        const input: InputFindCustomerDto = {
            id: '321',
        };

        expect(() => useCase.execute(input)).rejects.toThrowError('Customer not found');
    });
});