import { Sequelize } from "sequelize-typescript";
import { Customer } from "../../../domain/customer/entity/customer";
import { Address } from "../../../domain/customer/value-object/address";
import { CustomerModel } from "../../../infrastructure/customer/repository/sequelize/customer.model";
import { FindCustomerUseCase } from "./find.customer.usecase";
import { InputFindCustomerDto, OutputFindCustomerDto } from "./find.customer.dto";
import { CustomerRepository } from '../../../infrastructure/customer/repository/customer.repository';

describe('Test find customer use case', () => {
    let sequelize: Sequelize;

    beforeEach(async () => {
        sequelize = new Sequelize({
            dialect: 'sqlite',
            storage: ':memory:',
            logging: false,
            sync: { force: true },
        });

        sequelize.addModels([CustomerModel]);

        await sequelize.sync();
    });

    afterEach(async () => {
        await sequelize.close();
    });

    it('should find customer', async () => {
        const customer = new Customer('123', 'John Doe');
        const address = new Address('Street', 'City', 'Number', 'Zip');
        customer.changeAddress(address);

        const customerRepository = new CustomerRepository();
        await customerRepository.create(customer);

        const input: InputFindCustomerDto = {
            id: '123',
        };

        const useCase = new FindCustomerUseCase(customerRepository);

        const output: OutputFindCustomerDto = {
            id: '123',
            name: 'John Doe',
            address: {
                street: 'Street',
                city: 'City',
                number: 'Number',
                zip: 'Zip',
            },
        }

        const result: OutputFindCustomerDto = await useCase.execute(input);

        expect(result).toEqual(output);
    });
});