export type NotificationErrorProps = {
  message: string;
  context: string;
};

export class Notification {
  private _errors: NotificationErrorProps[] = [];

  public get errors(): NotificationErrorProps[] {
    return this._errors;
  }

  public addError(error: NotificationErrorProps) {
    this._errors.push(error);
  }

  public messages(context?: string): string {
    let message: string = "";

    this._errors.forEach((error: NotificationErrorProps) => {
      if (!context || error.context === context) {
        message += `${error.context}: ${error.message},`;
      }
    });

    return message;
  }

  hasErrors(): boolean {
    return this._errors.length > 0;
  }
}
