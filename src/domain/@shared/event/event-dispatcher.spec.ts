import { Customer } from "../../customer/entity/customer";
import { CustomerAddressChangedEvent } from "../../customer/event/customer/customer-address-changed.event";
import { CustomerCreatedEvent } from "../../customer/event/customer/customer-created.event";
import { SendConsoleLogWhenAddressWasChangedHandler } from "../../customer/event/customer/handler/send-console-log-when-address-was-changed.handler";
import { SendConsoleLog1WhenCustomerIsCreatedHandler } from "../../customer/event/customer/handler/send-console-log1-when-customer-was-created.handler";
import { SendConsoleLog2WhenCustomerIsCreatedHandler } from "../../customer/event/customer/handler/send-console-log2-when-customer-was-created.handler";
import { Address } from "../../customer/value-object/address";
import { SendEmailWhenProductWasCreatedHandler } from "../../product/event/handler/send-email-when-product-was-created.handler";
import { ProductCreatedEvent } from "../../product/event/product-created.event";
import { EventDispatcher } from "./event-dispatcher";

describe('Domain event tests', () => {

    it('should register an event handler', () => {
        const eventDispatcher = new EventDispatcher();
        const eventHandler = new SendEmailWhenProductWasCreatedHandler();

        eventDispatcher.register('ProductCreatedEvent', eventHandler);

        expect(eventDispatcher.getEventHandlers['ProductCreatedEvent']).toBeDefined();
        expect(eventDispatcher.getEventHandlers['ProductCreatedEvent'].length).toBe(1);
    });

    it('should unregister an event handler', () => {
        const eventDispatcher = new EventDispatcher();
        const eventHandler = new SendEmailWhenProductWasCreatedHandler();

        eventDispatcher.register('ProductCreatedEvent', eventHandler);
        eventDispatcher.unregister('ProductCreatedEvent', eventHandler);

        expect(eventDispatcher.getEventHandlers['ProductCreatedEvent']).toBeDefined();
        expect(eventDispatcher.getEventHandlers['ProductCreatedEvent'].length).toBe(0);
    });

    it('should unregister all event handlers', () => {
        const eventDispatcher = new EventDispatcher();
        const eventHandler = new SendEmailWhenProductWasCreatedHandler();

        eventDispatcher.register('ProductCreatedEvent', eventHandler);
        expect(eventDispatcher.getEventHandlers['ProductCreatedEvent']).toBeDefined();

        eventDispatcher.unregisterAll();
        expect(eventDispatcher.getEventHandlers['ProductCreatedEvent']).toBeUndefined();
    });

    it('should dispatch an event', () => {
        const eventDispatcher = new EventDispatcher();
        const eventHandler = new SendEmailWhenProductWasCreatedHandler();
        const spyEventHandler = jest.spyOn(eventHandler, 'handle');

        const productCreatedEvent = new ProductCreatedEvent({ name: 'Product 1' });

        eventDispatcher.register('ProductCreatedEvent', eventHandler);

        expect(eventDispatcher.getEventHandlers['ProductCreatedEvent'][0]).toMatchObject(eventHandler);

        eventDispatcher.dispatch(productCreatedEvent);

        expect(spyEventHandler).toHaveBeenCalled();
    });

    it('should dispatch an event when a customer is created', () => {
        const eventDispatcher = new EventDispatcher();
        const eventHandler1 = new SendConsoleLog1WhenCustomerIsCreatedHandler();
        const eventHandler2 = new SendConsoleLog2WhenCustomerIsCreatedHandler();
        const event = new CustomerCreatedEvent({ name: 'John' });

        const spyEventHandler1 = jest.spyOn(eventHandler1, 'handle');
        const spyEventHandler2 = jest.spyOn(eventHandler2, 'handle');

        eventDispatcher.register('CustomerCreatedEvent', eventHandler1);
        eventDispatcher.register('CustomerCreatedEvent', eventHandler2);

        expect(eventDispatcher.getEventHandlers['CustomerCreatedEvent'][0]).toMatchObject(eventHandler1);
        expect(eventDispatcher.getEventHandlers['CustomerCreatedEvent'][1]).toMatchObject(eventHandler2);

        eventDispatcher.dispatch(event);

        expect(spyEventHandler1).toHaveBeenCalled();
        expect(spyEventHandler2).toHaveBeenCalled();
    });

    it('should dispatch an event when a customer address was changed', () => {
        const eventDispatcher = new EventDispatcher();
        const eventHandler = new SendConsoleLogWhenAddressWasChangedHandler();

        const spyEventHandler = jest.spyOn(eventHandler, 'handle');

        eventDispatcher.register('CustomerAddressChangedEvent', eventHandler);

        const customer = new Customer('123', 'John');
        const address = new Address('street', 'city', 'number', 'zip');

        customer.changeAddress(address);

        const event = new CustomerAddressChangedEvent({ id: customer.id, name: customer.name, address: customer.address });

        eventDispatcher.dispatch(event);

        expect(eventDispatcher.getEventHandlers['CustomerAddressChangedEvent'][0]).toMatchObject(eventHandler);

        expect(spyEventHandler).toHaveBeenCalled();
    });

});