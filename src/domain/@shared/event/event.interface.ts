export interface EventInterface {
    ocurrenceDate: Date;
    eventData: any;
}