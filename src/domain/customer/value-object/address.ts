export class Address {
    private _street: string;
    private _city: string;
    private _number: string;
    private _zip: string;

    constructor(street: string, city: string, number: string, zip: string) {
        this._street = street;
        this._city = city;
        this._number = number;
        this._zip = zip;

        this.validate();
    }

    public get street(): string {
        return this._street;
    }

    public get city(): string {
        return this._city;
    }

    public get number(): string {
        return this._number;
    }

    public get zip(): string {
        return this._zip;
    }

    private validate(): void {
        if (!this._street) {
            throw new Error('Address must have a street');
        }

        if (!this._city) {
            throw new Error('Address must have a city');
        }

        if (!this._number) {
            throw new Error('Address must have a number');
        }

        if (!this._zip) {
            throw new Error('Address must have a zip code');
        }
    }

    toString(): string {
        return `${this._street}, ${this._number}, ${this._city}, ${this._zip}`;
    }
}