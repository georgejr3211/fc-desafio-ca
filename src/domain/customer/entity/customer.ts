import { Entity } from "../../@shared/entity/entity.abstract";
import { NotificationError } from "../../@shared/notification/notification.error";
import { CustomerValidatorFactory } from "../factory/customer.validator.factory";
import { Address } from "../value-object/address";

export class Customer extends Entity {
  private _name: string;
  private _address: Address;
  private _active: boolean;
  private _rewardPoints: number = 0;

  constructor(id: string, name: string) {
    super();
    this._id = id;
    this._name = name;

    this.validate();

    if (this.notification.hasErrors()) {
      throw new NotificationError(this.notification.errors);
    }
  }

  public get name(): string {
    return this._name;
  }

  public get address(): Address {
    return this._address;
  }

  get rewardPoints(): number {
    return this._rewardPoints;
  }

  private validate(): void {
    CustomerValidatorFactory.create().validate(this);
  }

  public changeName(name: string): void {
    this._name = name;
    this.validate();
  }

  public isActive(): boolean {
    return !!this._active;
  }

  public changeAddress(address: Address): void {
    this._address = address;
    this.validate();
  }

  public activate(): void {
    if (!this._address) {
      throw new Error("Customer must have an address to be activated");
    }

    this._active = true;
  }

  public deactivate(): void {
    this._active = false;
  }

  public addRewardPoints(points: number): void {
    this._rewardPoints += points;
  }
}
