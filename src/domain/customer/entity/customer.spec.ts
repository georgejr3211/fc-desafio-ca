import { Notification } from "../../@shared/notification/notification";
import { Address } from "../value-object/address";
import { Customer } from "./customer";

describe("Customer unit test", () => {
  it("should throw error when id is empty", () => {
    expect(() => {
      new Customer("", "John Doe");
    }).toThrowError("customer: must have an id");
  });

  it("should throw error when name is empty", () => {
    expect(() => {
      new Customer("123", "");
    }).toThrowError("customer: must have a name");
  });

  it("should change name", () => {
    const customer = new Customer("123", "John Doe");
    customer.changeName("Jane Doe");
    expect(customer.name).toBe("Jane Doe");
  });

  it("should throw error when name and id is empty", () => {
    expect(() => {
      new Customer("", "");
    }).toThrowError("customer: must have an id,customer: must have a name");
  });

  it("should throw error when activating customer without address", () => {
    const customer = new Customer("123", "John Doe");
    expect(() => {
      customer.activate();
    }).toThrowError("Customer must have an address to be activated");
  });

  it("should activate customer", () => {
    const customer = new Customer("123", "John Doe");
    const address = new Address("street", "city", "state", "zip");

    customer.changeAddress(address);
    customer.activate();

    expect(customer.isActive()).toBeTruthy();
  });

  it("should deactivate customer", () => {
    const customer = new Customer("123", "John Doe");

    customer.deactivate();

    expect(customer.isActive()).toBeFalsy();
  });

  it("should add reward points", () => {
    const customer = new Customer("123", "John Doe");
    expect(customer.rewardPoints).toBe(0);

    customer.addRewardPoints(10);
    expect(customer.rewardPoints).toBe(10);

    customer.addRewardPoints(10);
    expect(customer.rewardPoints).toBe(20);
  });

  it("should check if notification has at least one error", () => {
    const notification = new Notification();
    const error = {
      message: "error message 1",
      context: "customer",
    };
    notification.addError(error);

    expect(notification.hasErrors()).toBeTruthy();
  });
});
