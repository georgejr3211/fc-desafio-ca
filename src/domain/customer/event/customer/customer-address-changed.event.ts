import { EventInterface } from "../../../@shared/event/event.interface";

export class CustomerAddressChangedEvent implements EventInterface {
    ocurrenceDate: Date;
    eventData: any;

    constructor(eventData: any) {
        this.ocurrenceDate = new Date();
        this.eventData = eventData;
    }
}