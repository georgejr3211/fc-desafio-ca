import { EventHandlerInterface } from '../../../../@shared/event/event-handler.interface';
import { EventInterface } from '../../../../@shared/event/event.interface';
import { CustomerAddressChangedEvent } from '../customer-address-changed.event';

export class SendConsoleLogWhenAddressWasChangedHandler implements EventHandlerInterface<CustomerAddressChangedEvent> {
    handle(event: EventInterface): void {
        const { eventData } = event;
        console.log(`Endereço do cliente: ${eventData.id}, ${eventData.name} alterado para: ${eventData.address.toString()}`);
    }
}