import { OrderItem } from "./order-item";

export class Order {
    private _id: string;
    private _customerId: string;
    private _items: OrderItem[];

    constructor(id: string, customerId: string, items: OrderItem[]) {
        this._id = id;
        this._customerId = customerId;
        this._items = items;

        this.validate();
    }

    get id(): string {
        return this._id;
    }

    get customerId(): string {
        return this._customerId;
    }

    get items(): OrderItem[] {
        return this._items;
    }

    private validate(): void {
        if (!this._id) {
            throw new Error('Order must have an id');
        }

        if (!this._customerId) {
            throw new Error('Order must have a customer id');
        }

        if (!this._items || this._items.length === 0) {
            throw new Error('Order must have at least one item');
        }

        if (this._items.some(item => item.quantity <= 0)) {
            throw new Error('Order item quantity must be greater than zero');
        }
    }

    public total(): number {
        return this._items.reduce((total, item) => total + item.price, 0);
    }
}