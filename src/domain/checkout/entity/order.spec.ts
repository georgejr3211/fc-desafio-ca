import { Order } from './order';
import { OrderItem } from './order-item';

describe('Order unit test', () => {

    it('should throw error when id is empty', () => {
        expect(() => {
            new Order('', '123', []);
        }).toThrowError('Order must have an id');
    });

    it('should throw error when customer id is empty', () => {
        expect(() => {
            new Order('123', '', []);
        }).toThrowError('Order must have a customer id');
    });

    it('should throw error when items is empty', () => {
        expect(() => {
            new Order('123', '123', []);
        }).toThrowError('Order must have at least one item');
    });

    it('should throw error when item quantity is zero', () => {
        expect(() => {
            new Order('123', '123', [
                new OrderItem('123', 'product1', 10, '123', 0)
            ]);
        }).toThrowError('Order item quantity must be greater than zero');
    });

    it('should calculate total', () => {
        const order = new Order('123', '123', [
            new OrderItem('1', 'product1', 10, 'p1', 1),
            new OrderItem('2', 'product2', 20, 'p2', 2),
            new OrderItem('3', 'product3', 30, 'p3', 3),
        ]);

        expect(order.total()).toEqual(140);
    });

});