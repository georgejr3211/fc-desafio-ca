import { Order } from '../entity/order';
import { OrderItem } from '../entity/order-item';
import { OrderFactory } from './order.factory';

describe('Order factory unit test', () => {

    it('should create an order', () => {
        const item1 = new OrderItem('123', 'Product 1', 10, '1', 10);
        const order = OrderFactory.createOrder('123', [item1]);
        expect(order).toBeInstanceOf(Order);
    });

});