import { v4 as uuid } from "uuid";
import { Order } from "../entity/order";
import { OrderItem } from "../entity/order-item";

export class OrderFactory {
    static createOrder(customerId: string, items: OrderItem[]): Order {
        return new Order(uuid(), customerId, items);
    }
}