import { ValidatorInterface } from "../../@shared/validator/validator.interface";
import { Product } from "../entity/product";
import * as yup from "yup";

export class ProductYupValidator implements ValidatorInterface<Product> {
  async validate(entity: Product): Promise<void> {
    try {
      yup
        .object()
        .shape({
          id: yup.string().required("id is required"),
          name: yup.string().required("name is required"),
          price: yup.number().moreThan(0, 'price is required'),
        })
        .validateSync(
          {
            id: entity.id,
            name: entity.name,
            price: entity.price,
          },
          {
            abortEarly: false,
          }
        );
    } catch (errors) {
      const e = errors as yup.ValidationError;
      e.errors.forEach((error) => {
        entity.notification.addError({
          context: "product",
          message: error,
        });
      });
    }
  }
}
