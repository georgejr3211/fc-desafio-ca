import { EventInterface } from "../../@shared/event/event.interface";

export class ProductCreatedEvent implements EventInterface {
    ocurrenceDate: Date;
    eventData: any;

    constructor(eventData: any) {
        this.eventData = eventData;
        this.ocurrenceDate = new Date();
    }
}