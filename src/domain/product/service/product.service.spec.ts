import { Product } from "../entity/product";
import { ProductService } from "./product.service";

describe('Product Service unit test', () => {

    it('should change the price of all products', () => {
        const product1 = new Product('1', 'product1', 10);
        const product2 = new Product('2', 'product2', 20);
        const product3 = new Product('3', 'product3', 30);

        const products = [product1, product2, product3];

        ProductService.increasePrice(products, 100);

        expect(product1.price).toBe(20);
        expect(product2.price).toBe(40);
        expect(product3.price).toBe(60);
    });

});