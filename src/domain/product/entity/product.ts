import { Entity } from '../../@shared/entity/entity.abstract';
import { NotificationError } from '../../@shared/notification/notification.error';

export class Product extends Entity {
  private _name: string;
  private _price: number;

  constructor(id: string, name: string, price: number) {
    super();

    this._id = id;
    this._name = name;
    this._price = price;

    this.validate();

    if (this.notification.hasErrors()) {
      throw new NotificationError(this.notification.errors);
    }
  }

  public get name(): string {
    return this._name;
  }

  public get price(): number {
    return this._price;
  }

  private validate(): void {
    if (!this._id) {
      this.notification.addError({
        context: "product",
        message: "id is required",
      });
    }

    if (!this._name) {
      this.notification.addError({
        context: "product",
        message: "name is required",
      });
    }

    if (this._price <= 0) {
      this.notification.addError({
        context: "product",
        message: "price is required",
      });
    }
  }

  public changeName(name: string): void {
    this._name = name;
    this.validate();
  }

  public changePrice(price: number): void {
    this._price = price;
    this.validate();
  }
}
