import { Product } from "./product";

describe("Product unit test", () => {
  it("should throw error when id is empty", () => {
    expect(() => {
      new Product("", "product1", 10);
    }).toThrowError("product: id is required");
  });

  it("should throw error when name is empty", () => {
    expect(() => {
      new Product("123", "", 10);
    }).toThrowError("product: name is required");
  });

  it("should throw error when price is empty", () => {
    expect(() => {
      new Product("123", "product1", 0);
    }).toThrowError("product: price is required");
  });

  it("should throw error when id, name and price is empty", () => {
    expect(() => {
      new Product("", "", 0);
    }).toThrowError(
      "product: id is required,product: name is required,product: price is required"
    );
  });

  it("should change name", () => {
    const product = new Product("123", "product1", 10);
    product.changeName("product2");

    expect(product.name).toEqual("product2");
  });
});
